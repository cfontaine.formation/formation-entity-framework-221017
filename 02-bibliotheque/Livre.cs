﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_bibliotheque
{
    [Table("livres")]
    internal class Livre
    {
        public long Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Titre { get; set; }

        [Column("annee_sortie")]
        public int AnneeSortie { get; set; }

        public Livre(string titre, int anneeSortie)
        {
            Titre = titre;
            AnneeSortie = anneeSortie;
        }

        public override string ToString()
        {
            return $"Livre[{Id} {Titre} {AnneeSortie}]";
        }
    }
}
