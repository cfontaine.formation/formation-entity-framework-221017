﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_bibliotheque
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using(var ctx=new BiblioContext())
            {
                  Nation belgique = new Nation("Belgique");
                  ctx.Nations.Add(belgique);
                  ctx.SaveChanges();
            }
        }
    }
}
