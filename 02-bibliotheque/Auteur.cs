﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_bibliotheque
{
    [Table("auteurs")]
    internal class Auteur
    {
        public long Id{get;set;}

        [Required]
        [MaxLength(50)]
        public string Prenom { get; set; }

        [Required]
        [MaxLength(50)]
        public string Nom { get; set; }

        [Required]
        [Column(TypeName ="Datetime2")]
        public DateTime Naissance { get; set; }

        [Column(TypeName = "Datetime2")]
        public DateTime Deces { get; set; }

        public Auteur(string prenom, string nom, DateTime naissance)
        {
            Prenom = prenom;
            Nom = nom;
            Naissance = naissance;
        }

        public Auteur(string prenom, string nom, DateTime naissance, DateTime deces) : this(prenom, nom, naissance)
        {
            Deces = deces;
        }

        public override string ToString()
        {
            return $"Auteur[{Id} {Prenom} {Nom} {Naissance} {Deces}]";
        }
    }
}
