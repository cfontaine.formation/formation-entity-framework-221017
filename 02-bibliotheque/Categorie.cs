﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_bibliotheque
{
    [Table("categories")]
    internal class Categorie
    {
        public long Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Nom { get; set; }

        public Categorie(string nom)
        {
            Nom = nom;
        }

        public override string ToString()
        {
            return $"Categorie[{Id}, {Nom}]";
        }
    }
}
