﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_bibliotheque
{
    internal class BiblioContext : DbContext
    {
        public BiblioContext() : base("ChaineCnx")
        {
        }
        public DbSet<Auteur> Auteurs { get; set; }
        public DbSet<Nation> Nations { get; set; }
        public DbSet<Livre> Livres { get; set; }
        public DbSet<Categorie> Categories { get; set; }
    }
}
