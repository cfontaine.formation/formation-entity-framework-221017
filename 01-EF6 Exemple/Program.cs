﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_EF6_Exemple
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using (var context =new CommerceContext())
            {
                Adresse adr = new Adresse("46, rue des canonniers", "Lille", "59800");
                Client cl1 = new Client("John", "Doe", new DateTime(1995, 3, 11), adr);
                Console.WriteLine(cl1);
                context.Clients.Add(cl1);
                context.SaveChanges();
                Console.WriteLine(cl1);
                Console.ReadKey();
            }
        }
    }
}
