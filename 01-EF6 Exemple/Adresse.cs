﻿namespace _01_EF6_Exemple
{
    public class Adresse
    {
        public long Id { get; set; }
        public string Rue { get; set; }
        public string Ville { get; set; }
        public string CodePostal { get; set; }

        public Adresse(string rue, string ville, string codePostal)
        {
            Rue = rue;
            Ville = ville;
            CodePostal = codePostal;
        }

        public override string ToString()
        {
            return $"{Id}{Rue} {Ville} {CodePostal}";
        }
    }
}