﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_EF6_Exemple
{
    internal class CommerceContext : DbContext
    {
        public CommerceContext() : base("MagasinDbCnxString")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // modelBuilder.Entity<Client>();
            modelBuilder.Entity<Client>().ToTable("Clients");
            modelBuilder.Entity<Client>()
                .Property(c => c.Prenom)
                .HasColumnName("prenom");

            modelBuilder.Entity<Client>() 
                .Property(c => c.Nom)
                .IsRequired(); // Not null


            modelBuilder.Entity<Client>()
                .HasKey(c => c.PkClient);
            base.OnModelCreating(modelBuilder);
        }

         public DbSet<Client> Clients { get; set; }
    }
}
