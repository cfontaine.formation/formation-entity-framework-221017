﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_EF6_Exemple
{
    [Table("clients")]
    internal class Client
    {
        // Id -> nom de la clé primaire par défaut
        // ClientId
        //[Key]
        public long PkClient { get; set; }

        [Required]
        public string Prenom { get; set; }

        [Column("nom")]
        [MaxLength(50)]
        public string Nom { get; set; }

        //[Column("date_naissance",TypeName ="DateTime2")]
        public DateTime DateNaissance { get; set; }

        public Adresse Adresse { get; set; }

        public Client(string prenom, string nom, DateTime dateNaissance, Adresse adresse)
        {
            Prenom = prenom;
            Nom = nom;
            DateNaissance = dateNaissance;
            Adresse = adresse;
        }

        public override string ToString()
        {
            return $"{PkClient}{Prenom} {Nom} {DateNaissance} {Adresse}";
        }
    }
}
